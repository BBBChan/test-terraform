variable "region" {
    type = string
    default = "us-west-2"
}

variable "vpc_cidr" {
    type = string
}

variable "name" {
  type = string
}

variable "public_cidr" {
    type = list(string)
}

variable "private_cidr" {
    type = list(string)
}

variable "custom_cidr" {
    type = list(string)
}

variable "az" {
    type = list(string)
}

variable "custom_rta" {
type = map(object({
    subnet_id = string
    route_table_id = string
    }))
}

variable "custom_route" {
  type = map(object({
    cidr_block = string
    carrier_gateway_id = string
    destination_prefix_list_id = string
    egress_only_gateway_id = string
    gateway_id = string
    instance_id = string
    ipv6_cidr_block = string
    local_gateway_id = string
    nat_gateway_id = string
    network_interface_id = string
    transit_gateway_id = string
    vpc_endpoint_id = string
    vpc_peering_connection_id = string
    }))
}
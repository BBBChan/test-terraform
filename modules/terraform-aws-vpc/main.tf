#vpc
resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr
  enable_dns_support = true
  enable_dns_hostnames = true 

  tags = {
    Name = var.name
  }
}

#public subnet
resource "aws_subnet" "public" {
  vpc_id = aws_vpc.vpc.id
  count = length(var.public_cidr)
  cidr_block = element(var.public_cidr, count.index)
  availability_zone = element(var.az, count.index)
  map_public_ip_on_launch = true

  tags = {
    Name = "${element(var.az, count.index)}-public"
  }
}

#private subnets
resource "aws_subnet" "private" {
  vpc_id = aws_vpc.vpc.id
  count = length(var.private_cidr)
  cidr_block = element(var.private_cidr, count.index)
  availability_zone = element(var.az, count.index)
  map_public_ip_on_launch = false

  tags = {
    Name = "${element(var.az, count.index)}-private"
  }
}

#custom subnet
resource "aws_subnet" "custom" {
  vpc_id = aws_vpc.vpc.id
  count = length(var.custom_cidr)
  cidr_block = element(var.custom_cidr, count.index)
  availability_zone = element(var.az, count.index)

  tags = {
    Name = "${element(var.az, count.index)}-custom"
  }
}

#igw
resource "aws_internet_gateway" "test-igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "${var.name}-igw"
  }
}

#eip use for nat gw
resource "aws_eip" "nat-eip" {
  vpc        = true
  depends_on = [aws_internet_gateway.test-igw]
}

#NAT gw
resource "aws_nat_gateway" "test-nat" {
  allocation_id = aws_eip.nat-eip.id
  subnet_id = element(aws_subnet.public.*.id, 0)
  depends_on = [aws_internet_gateway.test-igw]
  tags = {
    Name = "${var.name}-nat"
  }
}

#public subnets route table
resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.test-igw.id
  }

  tags = {
    Name = "${var.name}-public-route-table"
  }
}

#associate subnet and public route table
resource "aws_route_table_association" "public-igw-rta" {
  count = length(var.public_cidr)
  subnet_id = element(aws_subnet.public.*.id, count.index)
  route_table_id = aws_route_table.public_rt.id
}

#private subnet route table
resource "aws_route_table" "private-rt" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.test-nat.id
  }

  tags = {
    Name = "${var.name}-private-route-table"
  }
}

#associate private subnet and public route table
resource "aws_route_table_association" "private" {
  count          = length(var.private_cidr)
  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = aws_route_table.private-rt.id
}

#custom subnet route table
resource "aws_route_table" "custom-rt" {
  for_each = var.custom_route

  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = each.value.cidr_block
    carrier_gateway_id = each.value.carrier_gateway_id
    destination_prefix_list_id = each.value.destination_prefix_list_id
    egress_only_gateway_id = each.value.egress_only_gateway_id
    gateway_id = each.value.gateway_id
    instance_id = each.value.instance_id
    ipv6_cidr_block = each.value.ipv6_cidr_block
    local_gateway_id = each.value.local_gateway_id
    nat_gateway_id = each.value.nat_gateway_id
    network_interface_id = each.value.network_interface_id
    transit_gateway_id = each.value.transit_gateway_id
    vpc_endpoint_id = each.value.vpc_endpoint_id
    vpc_peering_connection_id = each.value.vpc_peering_connection_id
  }
  tags = {
    Name = "${each.key}-custom-route-table"
  }
}

#custom route table associate
resource "aws_route_table_association" "custom_rta" {
  for_each = var.custom_rta

  subnet_id = each.value.subnet_id
  route_table_id = each.value.route_table_id
}

#vpc security group
resource "aws_security_group" "test-sg" {
    vpc_id = "${aws_vpc.vpc.id}"

    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
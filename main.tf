#create public subnets
module "public" {
  source = "./modules/terraform-aws-vpc"
  vpc_cidr = "10.0.0.0/16"
  name = "test-vpc"
  public_cidr = ["10.0.1.0/24"]
  private_cidr  = []
  custom_cidr = []
  az = ["us-west-2a"]
  custom_rta = {}
  custom_route = {}
}

#create private subnets

module "private" {
  source = "./modules/terraform-aws-vpc"
  vpc_cidr = "10.0.0.0/16"
  name = "test-vpc"
  public_cidr = ["10.0.1.0/24"]
  private_cidr = ["10.0.101.0/24"]
  custom_cidr = []
  az = ["us-west-2a"]
  custom_rta = {}
  custom_route = {}
}

#create custom subnet
module "custom" {
  source = "./modules/terraform-aws-vpc"
  vpc_cidr = "10.0.0.0/16"
  name = "test-vpc"
  public_cidr = ["10.0.1.0/24"]
  private_cidr = []
  custom_cidr = ["10.0.2.0/24", "10.0.3.0/24"]
  az = ["us-west-2a"]
  custom_route = {
    # custom_route1 = {
    #   cidr_block = "0.0.0.0/0"
    #   carrier_gateway_id = ""
    #   destination_prefix_list_id = ""
    #   egress_only_gateway_id = ""
    #   gateway_id = ""
    #   instance_id = ""
    #   ipv6_cidr_block = ""
    #   local_gateway_id = ""
    #   nat_gateway_id = ""
    #   network_interface_id = ""
    #   transit_gateway_id = ""
    #   vpc_endpoint_id = ""
    #   vpc_peering_connection_id = ""
    # }
    # custom_route2 = {
    #   cidr_block = "0.0.0.0/0"
    #   carrier_gateway_id = ""
    #   destination_prefix_list_id = ""
    #   egress_only_gateway_id = ""
    #   gateway_id = ""
    #   instance_id = ""
    #   ipv6_cidr_block = ""
    #   local_gateway_id = ""
    #   nat_gateway_id = ""
    #   network_interface_id = ""
    #   transit_gateway_id = ""
    #   vpc_endpoint_id = ""
    #   vpc_peering_connection_id = ""
    # }
  }
custom_rta = {
    # custom_rta1 = {
    #   subnet_id = ""
    #   route_table_id = ""
    # }
  }
}
# terraform-vac

The project will create VPC, public/private/custom subnets, internet gateway, NAT gateway (with EIP), related route table and security group. Please note that NAT gateway will charge fee while create. 

Please first run the below command to init project.

```
terraform init
```

## Level 1

Create public subnet, internet gateway, and route between then.

```
terraform apply --auto-approve -target module.public
```

In main.tf, the following configuaration is used and can be editable for further configure.

```
module "public" {
  source = "./modules/terraform-aws-vpc"
  vpc_cidr = "10.0.0.0/16"
  name = "test-vpc"
  public_cidr = ["10.0.1.0/24"]
  private_cidr  = []
  custom_cidr = []
  az = ["us-west-2a"]
  custom_rta = {}
  custom_route = {}
}
```

## Level 2

Create private subnet, nat gateway, and route between then. Due to NAT gatewat setting, it is suggested to contain at least one public subnet.

```
terraform apply --auto-approve -target module.private
```

In main.tf, the following configuaration is used and can be editable for further configure.

```
module "private" {
  source = "./modules/terraform-aws-vpc"
  vpc_cidr = "10.0.0.0/16"
  name = "test-vpc"
  public_cidr = ["10.0.1.0/24"]
  private_cidr = ["10.0.101.0/24"]
  custom_cidr = []
  az = ["us-west-2a"]
  custom_rta = {}
  custom_route = {}
}
```

## Level 3

Create custom subnet, internet gateway, NAT gateway. Due to NAT gatewat setting, it is suggested to contain at least one public subnet.

Level3 is divided into three parts:
1. Create custom subnet, internet gateway, NAT gateway, etc resources
2. Create route table by gateway ID
3. Associate Subnet ID and Route Table ID

The first part can done by the following command:
```
terraform apply --auto-approve -target module.custom
```

In main.tf, the following configuaration is used and can be editable for further configure. 
```
module "custom" {
  source = "./modules/terraform-aws-vpc"
  vpc_cidr = "10.0.0.0/16"
  name = "test-vpc"
  public_cidr = ["10.0.1.0/24"]
  private_cidr = []
  custom_cidr = ["10.0.2.0/24", "10.0.3.0/24"]
  az = ["us-west-2a"]
  custom_route = {
    # custom_route1 = {
    #   cidr_block = "0.0.0.0/0"
    #   carrier_gateway_id = ""
    #   destination_prefix_list_id = ""
    #   egress_only_gateway_id = ""
    #   gateway_id = ""
    #   instance_id = ""
    #   ipv6_cidr_block = ""
    #   local_gateway_id = ""
    #   nat_gateway_id = ""
    #   network_interface_id = ""
    #   transit_gateway_id = ""
    #   vpc_endpoint_id = ""
    #   vpc_peering_connection_id = ""
    # }
    # custom_route2 = {
    #   cidr_block = "0.0.0.0/0"
    #   carrier_gateway_id = ""
    #   destination_prefix_list_id = ""
    #   egress_only_gateway_id = ""
    #   gateway_id = ""
    #   instance_id = ""
    #   ipv6_cidr_block = ""
    #   local_gateway_id = ""
    #   nat_gateway_id = ""
    #   network_interface_id = ""
    #   transit_gateway_id = ""
    #   vpc_endpoint_id = ""
    #   vpc_peering_connection_id = ""
    # }
  }
  custom_rta = {}
}
```

Next, user can create their own route by create object 'custom_route'. Route to igw and nat gw can be created after nat gw and igw is created. Due to each new route, a new route table is created.

At last, once subnet and desired route is created. User can input object into custom_rta to assocaite subnet and route table. For example:
```
custom_rta = {
    custom_rta1 = {
      subnet_id = "subnet-05de29d56b522cf5e"
      route_table_id = "rtb-0acee161ff8b6f07c"
    }
    custom_rta2 = {
      subnet_id = "subnet-0cedceda6a563002a"
      route_table_id = "rtb-0987d968f660bae74"
    }
  }
```
***The gateway ID, subnet ID and route table ID can both check on AWS panel.
